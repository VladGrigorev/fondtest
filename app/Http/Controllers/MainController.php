<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Feeds;
use View;

class MainController extends Controller {

    public function mainPage() {
        $feed = Feeds::make('https://lenta.ru/rss');
        $data = array(
            'title' => $feed->get_title(),
            'permalink' => $feed->get_permalink(),
            'items' => $feed->get_items()
        );

        return View::make('main', $data);
    }

    public function addComment(Request $request) {

        try {
            $comment = new Comment();
            $comment->timestamps = false;
            $comment->email = $request->input('email');
            $comment->text = $request->input('text');
            $comment->guid = $request->input('guid');
            $comment->save();
        } catch (\Exception $e) {
            return response()->json(
                            ['error' => $e->getMessage(), 'message' => get_class($e)]
            );
        }

        return response()->json(
                        ['status' => 'ok'], 200
        );
    }

    public function getComments(Request $request) {
        try {
            $comments = Comment::where('guid', $request->input('guid'))->get();
        } catch (\Exception $e) {
            return response()->json(
                            ['error' => $e->getMessage(), 'message' => get_class($e)]
            );
        }
        return response()->json(
                        ['status' => 'ok', 'comments' => $comments, 'guid' => $request->input('guid')], 200
        );
    }

}
