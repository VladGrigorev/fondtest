@extends('layouts.front')

@section('content')
<div class="row header">
    <div class="row">
        <div class="col-xs-11 col-xs-offset-1"><h1><a href="{{ $permalink }}">{{ $title }}</a></h1></div>
    </div>
</div>
@foreach ($items as $id => $item)
<div class="row item-{{$id}}">
    <div class="row">
        <div class="col-xs-6 col-xs-offset-1">
            <h2><a href="{{ $item->get_permalink() }}">{{ $item->get_title() }}</a></h2>
            <p><img src="{{ $item->get_enclosure()->get_link() }}"/></p>
            <p>{{$item->get_item_tags(SIMPLEPIE_NAMESPACE_RSS_20, 'description')[0]['data']}}</p>
            <p><small>Posted on {{ $item->get_date('j F Y | g:i a') }}</small></p>
        </div>
    </div>
    <div class="row">  
        <div class="col-xs-6 col-xs-offset-1">
            <h3>Оставить комментарий</h3>
            <form data-guid="{{$item->get_id()}}" id="commentForm">
                <div class="form-group">
                    <label><h4>E-mail:</h4></label>
                    <input type="text" class="form-control" id="userEmail">
                    <label><h4>Текст:</h4></label>
                    <textarea rows="5" class="form-control" id="addComment"></textarea>
                </div>
                <button type="submit" data-guid="{{$item->get_id()}}" class="btn btn-primary">Отправить</button>
            </form>
        </div>
    </div>
    <div class="row">  
        <a href="" id="showComments" data-guid-comments="{{$item->get_id()}}" class="col-xs-6 col-xs-offset-1">Показать комментарии</a> 
        <div data-guid-container="{{$item->get_id()}}" hidden="true" class="col-xs-6 col-xs-offset-1" id="comments">
            <h3>Комментарии</h3>  
        </div>
    </div>
</div>
<hr>
@endforeach
@endsection
@section('scripts')
<script type="text/javascript">
    $('*[data-guid]').on('submit', [], function (e) {
        e.preventDefault();
        let email = $(this).find("#userEmail").val();
        let text = $(this).find("#addComment").val();
        let guid = $(this).data("guid");
        fetch("{{route('addComment')}}", {
            method: 'post',
            credentials: "same-origin",
            dataType: 'json',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                text: text,
                guid: guid,
                _token: window.Laravel.csrfToken
            }),
        })
                .then((response) => {
                    if (response.ok) {
                        console.log('success!');
                        return response.json();
                    }

                    return response.json();
                })
                .then((json) => {
                    if (json['status'] == 'ok') {
                        console.log('test');
                        if(!$('*[data-guid-container="' + guid + '"]').is(":hidden")){
                        $('*[data-guid-container="' + guid + '"]').append('<h4>E-mail:<br>' + email + '</h4>\n\
                                        <h4>Текст сообщения:<br>' + text + '</h4>');
                        }
                        console.log(json);
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
    });
    $('*[data-guid-comments]').on('click', function (e) {
        e.preventDefault();
        let guid = $(this).data("guid-comments");
        console.log($(this).data("guid-comments"));
        fetch("{{route('getComments')}}", {
            method: 'post',
            credentials: "same-origin",
            dataType: 'json',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                guid: guid,
                _token: window.Laravel.csrfToken
            }),
        })
                .then((response) => {
                    if (response.ok) {
                        console.log('success!');
                        return response.json();
                    }
                    return response.json();
                })
                .then((json) => {
                    if (json['status'] == 'ok') {
                        json['comments'].forEach(function (item, i) {
                            console.log(i + ": " + item);
                            $('*[data-guid-container="' +item['guid'] + '"]').append('<h4>E-mail:<br>' + item['email'] + '</h4>\n\
                                        <h4>Текст сообщения:<br>' + item['text'] + '</h4>');
                        });
                        $('*[data-guid-container="' +json['guid'] + '"]').show(1, "linear");
                        $(this).hide(1, "linear");
                    }
                })
                .catch((error) => {
                    console.log(error);
                });     
    });
</script>
@endsection

